const promise = require("promise");
const {  readFile, unlink } = require("fs/promises");

async function deletefiless(filename) {
    try {
      await unlink(`${filename}`);
      //console.log(`f${filename} is deleted `);
      return promise.resolve();
    } catch (err) {
      console.log(err);
      return promise.reject();
    }
  }

async function deletefileinfilesname2_5() {
    //Read the contents of filenames.txt and delete all the new files that are mentioned in that list concurrently
   
   try {
     let fileData = await readFile(`./filenames.txt`, "utf8", "r");
     let filenames = [];
     filenames = fileData.split(/\n/);
     promiseList = [];
     for (let filename of filenames) {
       if (filename != "") {
         promiseList.push(deletefiless(filename));
       }
     }
     return promise.all(promiseList);
   } catch (err) {
     console.log(err);
     return promise.reject();
   }
 }

 module.exports = deletefileinfilesname2_5