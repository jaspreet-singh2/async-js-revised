const promise = require("promise");
const { appendFile, readFile, writeFile,  } = require("fs/promises");

const datafromnewfiles = [];

async function readdatafromnewfiles(fileName) {
    try {
      let filedata = await readFile(`${fileName}`, "utf8", "r");
      if(filedata !='')
      datafromnewfiles.push(filedata);
      return promise.resolve(`read data from ${fileName}`);
    } catch (err) {
      console.log(err);
      return promise.reject();
    }
  }


async function readfile2_4() {
    //Read the new files, sort the content, write it out to a new file, called sorted.txt. Store the name of the new file in filenames.txt");
   try {
     let fileData = await readFile(`./filenames.txt`, "utf8", "r");
     let fileNames = fileData.split(/\n/);
     promiselist = [];
     for (filename of fileNames) {
       if (filename != "" && filename != "uppercase.txt") {
         promiselist.push(readdatafromnewfiles(filename));
       }
     }
 
     await promise.all(promiselist);
 
     let sortedatafromnewfiles = datafromnewfiles.sort();
     let linewizesorted = sortedatafromnewfiles.join('\n');
 
     await writeFile(`./data/sorted.txt`, linewizesorted);
     await appendFile(`./filenames.txt`, `./data/sorted.txt\n`);
      console.log("done")
     return promise.resolve();
   } catch (err) {
     console.log(err);
     promise.reject();
   }
 }

 module.exports = readfile2_4